<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
	$lots = \App\Lots::all();
    return view('lots.index', compact('lots'));
});

Route::get('/contacts', function() {
  return view('contacts', compact('contacts'));
});

Route::resource('/lots', 'LotController');


Route::get('/home', 'HomeController@index');

Auth::routes();