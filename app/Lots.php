<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lots extends Model
{
    protected $fillable = [
    	'location',
    	'price',
    	'image',
    	'description'
    ];
}
