@extends('welcome')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container marketing">
				<div class="row featurette">
                	<div class="col-md-7">
						<h2><strong>This is how you can contact us:</strong></h2>
						<h3><span class="text-muted">Contact our administrator via email:</span> admin@admin.lt <span class="text-muted"> or telephone:</span> +370 (630) 00000</h3>
						</br>
						<h3><strong>You can also visit our main office!</strong></h3>
						<h3>Working hours: Monday to Friday 9:00 - 17:00</p></h3>
					</div>
				</div>
			</div>
			<div class="responsive-iframe"> 
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2306.7341207400427!2d25.265054216039804!3d54.67910798174803!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dd9472c09e8bb1%3A0x8d2b9e6b4a1aa84f!2s%C5%A0vitrigailos+g.+8%2C+Vilnius+03223!5e0!3m2!1sen!2slt!4v1481477688894" width="1170" height="750" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
	<hr class="featurette-divider">
</div>
@endsection