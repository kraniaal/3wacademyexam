@extends('welcome')
@section('content')
<div class="container">
    <div class="row">
	@if (Auth::user())   
	    @if(isset($lot))
	    <form class="form-group" action="{{ route('lots.update', $lot->id) }}" method="POST">
	    <input type="hidden" name="_method" value="PUT">
	    @else
	    <form class="form-group" action="{{ route('lots.store') }}" method="POST">
	    @endif
	        {{ csrf_field() }}
	        <fieldset>
	            <label for="location">Location:</label>
	            @if(isset($lot))
	            <input value="{{ $lot->location }}" class="form-control" type="text" name="location">
	            @else
	            <input class="form-control" type="text" name="location">
	            @endif
	            
	            <label for="price">Price:</label>
	            @if(isset($lot))
	            <input value="{{ $lot->price }}" class="form-control" type="text" name="price">
	            @else
	            <input class="form-control" type="text" name="price">
	            @endif

	            <label for="image">Image:</label>
	            @if(isset($lot))
	            <input value="{{ $lot->image }}" class="form-control" type="text" name="image">
	            @else
	            <input class="form-control" type="text" name="image">
	            @endif

	            <label for="description">Description:</label>
	            @if(isset($lot))
	            <input value="{{ $lot->description }}" class="form-control" type="text" name="description">
	            @else
	            <input class="form-control" type="text" name="description">
	            @endif
	            <button class="btn btn-success" type="submit" name="submit">{{ isset($lot) ? 'Edit' : 'Add'}}</button>
	        </fieldset>
	    </form>
	    
	    @if(isset($lot))
	    <form class="form-group" action="{{ route('lots.destroy', $lot->id) }}" method="POST">
	    {{ csrf_field() }}
	    <input type="hidden" name="_method" value="DELETE">
	        <button class="btn btn-danger">Delete</button>
	    </form>
	    @endif
	    </div>
	@else
	<h2>You're accessing the content made for admin only!</h2>
	<p>Please log in or try hacking somewhere else.</p>
	@endif
    <hr class="featurette-divider">

</div>
@endsection