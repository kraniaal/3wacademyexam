@extends('welcome')
@section('content')
		<div class="col-md-12">
			 <div class="container marketing">
            @foreach($lots as $lot)
              <div class="row featurette">
                <div class="col-md-7">
                  <h2 class="featurette-heading"> {{ $lot->location }} </br> <span class="text-muted"> {{ $lot->price }} &#8364; </span></h2>
                  <p class="lead"> {{ $lot->description }} m&sup2; </br> <small><span class="text-muted">Lot posted at: {{ $lot->created_at }} </span></small></p>
                </div>
                <div class="col-md-5">
                  <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="Generic placeholder image" src="{{ $lot->image }}">
                </div>
              </div>
              <a href="{{ url('contacts') }}" class="btn btn-success">Buy now!</a>
              @if (Auth::user())
                <a href="{{ route('lots.create') }}" class="btn btn-default">Create</a>
                <a href="{{ route('lots.edit', $lot->id) }}" class="btn btn-default">Edit</a>
              @endif
              <hr class="featurette-divider">
            @endforeach
        </div>
		</div>
@endsection