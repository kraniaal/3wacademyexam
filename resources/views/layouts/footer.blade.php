    <footer>
    <div id="footer-container" class="container">
        <div id="footer-row" class="row">
            <a href="{{ url('/login') }}" class="pull-right btn btn-default">Login</a>
            @if (Auth::user()) 
            <a href="{{ url('/logout') }}" class="pull-right btn btn-default" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            @endif
            <p>&copy; 2016 EasyLand, Inc. All rights reserved. &middot;</p>
        </div>
    </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    {{-- <script src="../../dist/js/bootstrap.min.js"></script> --}}
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    {{-- <script src="../../assets/js/vendor/holder.min.js"></script> --}}
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    {{-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> --}}

</body>
</html>