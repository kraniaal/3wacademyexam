<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Buy land lot online! Easy lots, cheap, fast, no fraud, guaranteed legal trade. Immovable property on mobile, pictures, accurate descriptions, real estates!">
    <meta name="author" content="Julius Lauciunas">
    {{-- <link rel="icon" href="../../favicon.ico"> --}}
    <title>EasyLand | Buy a land lot online!</title>
    <!-- Styles -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <!-- Latest compiled CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
  </head>
<!-- NAVBAR
================================================== -->
  <body>
        <div class="navbar-wrapper">
          <div class="container">

            <nav class="navbar navbar-inverse navbar-static-top">
              <div class="container">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="{{ url('lots') }}"><strong>EasyLand.eu</strong> - our best lots for you online!</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav pull-right">
                    <li><a href="{{ url('lots') }}">Choose your lot!</a></li>
                    <li><a href="{{ url('contacts') }}">Contacts</a></li>
                  </ul>
                </div>
              </div>
            </nav>

          </div>
        </div>


        <!-- Carousel
        ================================================== -->
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img class="first-slide" src="{{ asset('img/carousel-bg.jpg') }}" alt="First slide">
              <div class="container">
                <div class="carousel-caption">
                  <h1>Easy and cheap way to buy your new property!</h1>
                  <p>Browse through our current land lots below!</p>
                </div>
              </div>
            </div>
            <div class="item">
              <img class="second-slide" src="{{ asset('img/carousel-bg.jpg') }}" alt="Second slide">
              <div class="container">
                <div class="carousel-caption">
                  <h1>Just choose your favorite land lot!</h1>
                  <p>It's just THAT simple! Choose your new property and contact us. We will take care of everything else.</p>
                </div>
              </div>
            </div>
            <div class="item">
              <img class="third-slide" src="{{ asset('img/carousel-bg.jpg') }}" alt="Third slide">
              <div class="container">
                <div class="carousel-caption">
                  <h1>Browse on your PC, Tablet or mobile!</h1>
                  <p>Our website is optimized for hassle-free browsing throughout all platforms!</p>
                </div>
              </div>
            </div>
          </div>
          <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>