<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@admin.lt',
            'password' => Hash::make('administratorius'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }
}
