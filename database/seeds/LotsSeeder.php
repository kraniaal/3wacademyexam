<?php

use Illuminate\Database\Seeder;

class LotsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lots')->insert([
            'description' => '101',
            'image' => 'http://twinriversrealty.com/wp-content/uploads/2014/09/land-for-sale.jpg',
            'location' => 'In the middle of nowhere1',
            'price' => '5001',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        DB::table('lots')->insert([
            'description' => '102',
            'image' => 'http://twinriversrealty.com/wp-content/uploads/2014/09/land-for-sale.jpg',
            'location' => 'In the middle of nowhere2',
            'price' => '5002',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        DB::table('lots')->insert([
            'description' => '103',
            'image' => 'http://twinriversrealty.com/wp-content/uploads/2014/09/land-for-sale.jpg',
            'location' => 'In the middle of nowhere3',
            'price' => '5003',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }
}
