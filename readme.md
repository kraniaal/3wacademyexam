Database mods from .env file:
	DB_DATABASE=easyland
	DB_USERNAME=root
	DB_PASSWORD=

Migrations: 2016_12_05_093211_create_lot_table.php 2014_10_12_000000_create_users_table.php

Seeders used:
	LotsSeeder.php
	UserSeeder.php

Website is designed to have only one user for CRUD operations. You can access user content by loging in with a button from footer.
	Type these when prompted to do so (by clicking Login button from footer section):
	email: admin@admin.lt
	password: administratorius